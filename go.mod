module gitlab.com/cloudmicro/platform/microservices/natural-born-users/migrations.git

require (
	github.com/golang-migrate/migrate/v4 v4.7.0
	github.com/lib/pq v1.2.0
)
